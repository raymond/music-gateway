package main

import (
	"context"
	"fmt"
	"log"

	spotifyauth "github.com/zmb3/spotify/v2/auth"

	"golang.org/x/oauth2/clientcredentials"

	"github.com/zmb3/spotify/v2"
)

func stpofi() {
	ctx := context.Background()
	config := &clientcredentials.Config{
		ClientID:     "5f573c9620494bae87890c0f08a60293",
		ClientSecret: "212476d9b0f3472eaa762d90b19b0ba8",
		TokenURL:     spotifyauth.TokenURL,
	}
	token, err := config.Token(ctx)
	if err != nil {
		log.Fatalf("couldn't get token: %v", err)
	}

	httpClient := spotifyauth.New().Client(ctx, token)
	client := spotify.New(httpClient)

	playlist, err := client.GetPlaylist(ctx, "03Rp4VvSidaReMUG21Dx6z")
	if err != nil {
		log.Fatal(err)
	}

	// handle playlist results
	if playlist != nil {
		fmt.Println("Found:", playlist.Name, playlist.Owner.DisplayName)
	}
}
