package main

import (
	"encoding/json"

	"github.com/raitonoberu/ytmusic"
)

func yts_search(query string) string {
	s := ytmusic.Search(query)
	result, err := s.Next()
	if err != nil {
		panic(err)
	}

	jsonstr, _ := json.Marshal(result)
	return string(jsonstr)
}
